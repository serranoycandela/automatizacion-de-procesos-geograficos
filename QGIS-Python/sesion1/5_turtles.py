import turtle

liebre = turtle.Turtle()
liebre.forward(100)
liebre.width(4)
liebre.right(90)
liebre.color('red')
liebre.forward(110)
liebre.right(60)
liebre.color('green')
liebre.forward(200)
#%%



turtle.clearscreen()
turtle.bgcolor("black")

liebre = turtle.Turtle()
liebre.color('green')
liebre.width(4)
   
#%%

liebre.forward(100)
liebre.right(90)
#%%
def terreno_limpio():
    turtle.clearscreen()
    turtle.bgcolor("black")
    
def nueva_liebre(color):
    liebre = turtle.Turtle()
    liebre.color(color)
    liebre.width(4)
    return liebre  


terreno_limpio()
liebre = nueva_liebre("yellow")

for i in range(4):
    liebre.forward(100)
    liebre.right(90)
#%%

def poligono(n_lados):
    for i in range(n_lados):
        liebre.forward(100)
        liebre.right(360/n_lados)
        
terreno_limpio()
liebre = nueva_liebre("purple")
poligono(5)
#%%

#### actividad por equipos
### pintar un cuadrado, un pentágono, un hexágono y un heptágono de diferentes
### colores y en diferentes ubicaciones
###################################

# hint
terreno_limpio()
liebre = nueva_liebre("purple")
liebre.penup()
liebre.goto(-300,300)
liebre.pendown()
poligono(5)
#%%


#espiral amarillo
terreno_limpio()
tortuga = turtle.Turtle()
tortuga.color("yellow")
for x in range(200):
   tortuga.forward(x)
   tortuga.left(60-1)
#%%
   
# espiral de colores
colors = ["red", "purple", "blue", "green", "orange", "yellow"]
turtle.clearscreen()
turtle.bgcolor("black")
tortuga = turtle.Turtle()
for x in range(200):
   tortuga.color(colors[x % 6])
   tortuga.width(x/100 + 1)
   tortuga.forward(x)
   tortuga.left((360/6)-1)
#%%

