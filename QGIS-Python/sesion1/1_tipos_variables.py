#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 15 20:58:13 2022

@author: fidel
"""

a = 5 # variable tipo int
pi = 3.1416 # variable tipo float
pi = 3.14159 # variable tipo float
curso = "QGIS-Python" # variable tipo string

b = a + pi

cantidad = 3
precio = 150.5
print(cantidad * precio)
total = cantidad * precio

# suponiendo que el producto tiene un desuento de 50.5
descuento = 50.5
total = cantidad * precio - descuento # está mal
print(total) 

total = cantidad * (precio - descuento)
print(total)

nombre = "Arturo"
cumple = 35
print("Felicidades " + nombre)
print("Felicidades " + nombre + " por tu cumpleaños " + cumple) # marcará error
print("Felicidades " + nombre + " por tu cumpleaños " + str(cumple))

a = "3"
b = "1.5"
c = a / b #marcará error porque a y b son strings
c = int(a) / float(b)
print(c)
