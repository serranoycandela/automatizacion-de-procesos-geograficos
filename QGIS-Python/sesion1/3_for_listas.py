#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 15 23:03:49 2022

@author: fidel
"""
#################################### for ######################################
###############################################################################

# un for sencillo que imprime del 1 al 10
for n in range(1, 11):
    print(n)
#%%
    
# tablas de multiplicar:
# un for sencillo que nos da la tabla de multiplicar del número que digite el 
# usuario    
primer_numero = int(input("dame un múmero entero: "))
for segundo_numero in range(1, 11):
    multiplicacion = primer_numero * segundo_numero
    print(primer_numero, "*" , segundo_numero, "=", multiplicacion)
#%%
    
# un for anidado, es decir un for dentro de otro for que nos da las tablas de
# multiplicar del 1 al 10    
for primer_numero in range(1, 11):
    print("------- Tabla del ",primer_numero," ------")
    for segundo_numero in range(1, 11):
        multiplicacion = primer_numero * segundo_numero
        print(primer_numero, "*" , segundo_numero, "=", multiplicacion)
#%%


############################### Listas ########################################
###############################################################################

nombres = ["Luis","Rodrigo","Arturo","Monica","Ricardo","Paola"]
nombres[0] # el primer elemento
nombres[1] # el segundo elemento
nombres[-1] # el último elemento
nombres[-2] # el penúltimo elemento


# modificar elementos específicos
nombres[0] = "Fidel"
nombres[3] = "Mónica"
nombres

# obtener rebanadas de la lista
nombres[:3] # los primeros tres
nombres[2:5] # del tercero al quinto

# un for sobre la lista
for nombre in nombres:
    print(nombre)

# checar si un elemento pertenece a una lista
nombre = "Arturo"
if nombre in nombres:
    print(nombre)

# obtener el tamaño de la lista
len(nombres)
size = len(nombres)

# agregar un elemento    
nombres.append("Isabel")
nombres

# quitar un elemento
nombres.remove("Ricardo")
nombres

# ordenar la lista
nombres.sort()
nombres

# imprimir los nombres en mayúsculas
for nombre in nombres:
    print(nombre.upper())
