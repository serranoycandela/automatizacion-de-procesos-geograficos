#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 16 01:23:35 2022

@author: fidel
"""

# una funcion sencilla que recorre e imprime los números del 2 al número que 
# recibe
def factores_primos(numero):
    for divisor in range(2,numero+1):
        print(divisor)
            
        
factores_primos(8)
#%%


# que solo imprima el número si es un divisor
def factores_primos(numero):
    for divisor in range(2,numero+1):
        if numero % divisor == 0:
            print(divisor)
            
factores_primos(8)            
#%%

# hacer una lista con los divisores en vez de imprimir
def factores_primos(numero):
    factores = []
    for divisor in range(2,numero+1):
        if numero % divisor == 0:
            factores.append(divisor)
    return factores

factores_primos(8) 
factores_primos(10) 
#%%

#imprimir los factores primos de los primeros 30 números
for i in range(2,31):
    print(i,factores_primos(i))
#%%

#imprimir los factores primos de los primeros 30 números y resaltar los primos
for i in range(2,31):
    if len(factores_primos(i)) == 1:
        print(i,factores_primos(i), "------------------------------- Primo")
    else:
        print(i,factores_primos(i))
#%%

# una función que usa otra función
# imprime los factores primos de los primeros n números 
# y resalta los primos

def todos_hasta(n):
    for i in range(2,n+1):
        if len(factores_primos(i)) == 1:
            print(i,factores_primos(i), "------------------------------ Primo")
        else:
            print(i,factores_primos(i))
            
todos_hasta(10)
todos_hasta(20)
todos_hasta(100)
#%%