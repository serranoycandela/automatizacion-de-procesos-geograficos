# Recortar los 365 rasters a la zona de estudio
##################################################


import os
from qgis.core import QgsRasterLayer
import processing
        
carpeta = "/home/fidel/precipitacion/Daymet_Daily_V4/data/2020/"
os.chdir(carpeta) # cambiar el directorio de trabajo
print() # una linea en blanco
extent_layer = QgsVectorLayer("extent.shp", "extent", "ogr") # crea la capa como un objeto QgsVectorLayer 

for archivo in os.listdir(carpeta): # recorre los archivos en la carpeta
    if archivo.endswith(".tif"): # filtra los tif
        print(archivo)
        parameters = {
                'ALPHA_BAND': False,
                'CROP_TO_CUTLINE': True,
                'DATA_TYPE': 0,
                'INPUT': archivo,
                'KEEP_RESOLUTION': False,
                'MASK': extent_layer, # la misma capa de extent para todos
                'MULTITHREADING': False,
                'OPTIONS': '',
                'OUTPUT': './clipped/'+archivo, # el mismo que el raster original pero en la carpeta clipped
                'SET_RESOLUTION': False,
                'SOURCE_CRS': None,
                'TARGET_CRS': extent_layer.sourceCrs(), # la proyeccion de la capa extent
                'X_RESOLUTION': None,
                'Y_RESOLUTION': None
            }
        # correr el algoritmo de procesamiento con el diccionario de parametros
        resultado = processing.run("gdal:cliprasterbymasklayer", parameters)

