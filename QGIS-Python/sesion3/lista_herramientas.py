# lista los algoritmos de procesamiento disponibles
#######################################################

for alg in QgsApplication.processingRegistry().algorithms():
    print(alg.id(), "->", alg.displayName())

######## Ayuda para un algoritmo especifico
processing.algorithmHelp("native:extenttolayer") 
processing.algorithmHelp("native:zonalstatistics")    
processing.algorithmHelp("gdal:cliprasterbymasklayer")

###############################################
#                   Diccionarios
###############################################

ender_lista = ["Ender Wiggings", 10, 2, "Entender a su enemigo"]
print(ender_lista[0])
ender_dicc = {"Nombre" : "Ender Wiggings",
              "Edad": 10,
              "Hermanos": 2,
              "Especialidad": "Entender a su enemigo"
              }
fidel_dicc = {"Nombre" : "Fidel Serrano",
              "Edad" : 47,
              "Hermanos" : 1,
              "Especialidad" : "Acertijero"
              }

print(fidel_dicc["Nombre"])
print(fidel_dicc["Edad"])
fidel_dicc["Edad"] = 48
print(fidel_dicc["Edad"])


#############################################################
# usar la herramienta native:extenttolayer para hacer una 
# capa que tenga un solo poligono que sea el extent de los
# municipios
###########################################################
# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion3/capas/"
os.chdir(carpeta)
params = {'INPUT': 'municipios.shp',
          'OUTPUT': 'extent.shp'
          }
result = processing.run("native:extenttolayer", params)