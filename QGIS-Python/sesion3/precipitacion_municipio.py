# Calcular la precipitacion promedio de cada dia
# del 2020 para cada municipio
##################################################


import os
from qgis.core import QgsRasterLayer
import processing
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion3/"
os.chdir(carpeta) # cambiar el directorio de trabajo
print() # una linea en blanco
municipios = QgsVectorLayer("municipios_preci_2020.shp", "municipios", "ogr") # crea la capa como un objeto QgsVectorLayer 



list_of_files = sorted( filter( lambda x: os.path.isfile(x),
                        os.listdir(carpeta) ) )
contador = 0
for file_name in list_of_files:
    if file_name.endswith(".tif"):
        contador += 1
        print(contador, file_name)

contador = 0
for archivo in list_of_files: # recorre los archivos en la carpeta
    if archivo.endswith(".tif"):
        contador += 1
        nombre = archivo[:-4] # obtiene el nombre quitando la extension
        print(nombre)
        layer = QgsRasterLayer(archivo, nombre)
        
        prefix = "p_"+str(contador)+'_'
        params = {'INPUT_RASTER': layer,
            'RASTER_BAND': 1, 'INPUT_VECTOR': municipios,
            'COLUMN_PREFIX': prefix, 'STATISTICS': [2]
            }
        result = processing.run("native:zonalstatistics", params)