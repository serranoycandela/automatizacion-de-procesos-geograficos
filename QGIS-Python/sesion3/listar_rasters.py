# Hacer una funcion para obtener el minimo y el maximo de un Raster
# y listar las capas raster de una carpeta y sus respectivos ESPG y maximo y minimo
#################################################################

import os
from qgis.core import QgsVectorLayer


# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion3/"

def raster_min_max(layer):
    '''
    Esta funcion regresa los valores maximo y minimo de una capa raster
    con una sola banda
    '''
    provider = layer.dataProvider()
    stats = provider.bandStatistics(1,QgsRasterBandStats.All)

    v_min = stats.minimumValue
    v_max = stats.maximumValue
    return v_min, v_max

os.chdir(carpeta) # cambiar el directorio de trabajo
print() # una linea en blanco
for archivo in os.listdir(carpeta): # recorre los archivos en la carpeta
    if archivo.endswith(".tif"): # filtra los .tif
        nombre = archivo[:-4] # obtiene el nombre quitando la extension
        capa = QgsRasterLayer(archivo, nombre) # crea la capa como un objeto QgsRasterLayer 
        suESPG = capa.crs().authid() # obtiene el ESPG
        suTipo = capa.dataProvider().sourceDataType(1) # obtiene el tipo de raster
        suMin, suMax = raster_min_max(capa) # obtiene el minimo y el maximo
        print(nombre, suESPG,"  Tipo:", suTipo) # imprime la info de la capa
        print("    Min:", round(suMin,1), "  Max:", round(suMax,1))


