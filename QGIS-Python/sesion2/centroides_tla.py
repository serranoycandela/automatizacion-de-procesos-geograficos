# Crear una capa con los centroides de los municipios cuyo nombre contenga
# la secuencia de letras "tla"
##############################################################################

import os
from qgis.core import QgsVectorLayer


# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion2"

os.chdir(carpeta) # cambiar el directorio de trabajo
capa = QgsVectorLayer("municipios.shp","mun","ogr") # abrir la capa
centroides = QgsVectorLayer('Point?crs=epsg:32614','centroides',"memory") # Crear capa de memoria
centroides_provider = centroides.dataProvider() # obtener el data provider de la capa centroides
for elemento in capa.getFeatures(): # recorrer los elementos de la capa
    if "tla" in elemento['Nombre'].lower(): # filtrar los municipios cuyo nombre contenga "tla"
        geometria = elemento.geometry() # obtener la geometria
        centroide = geometria.centroid() # obtener el centroide de esa geometria
        print(elemento['Nombre']) # imprimir el nombre del municipio
        print("x =", centroide.asPoint().x()) # imprimir la coordenada x
        print("y =", centroide.asPoint().y()) # imprimir la coordenada y
        elemento_centroide = QgsFeature() # crear un elemento vacio
        elemento_centroide.setGeometry(centroide) # asignar las coordenadas del centroide
        centroides_provider.addFeatures([elemento_centroide]) # agregar el elemento a la capa centroides
centroides.updateExtents() # Actualizar la extension de la capa centroides
QgsProject.instance().addMapLayer(centroides) # Agregar la capa centroides al mapa
