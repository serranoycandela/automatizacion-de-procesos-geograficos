# Imrimir el porcentaje de bosques y selvas para cada municipio
########################################################

import os
from qgis.core import QgsVectorLayer


# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion2"

os.chdir(carpeta) # cambiar el directorio de trabajo
municipios = QgsVectorLayer("municipios.shp","mun","ogr") # abrir la capa de municipios
serie6 = QgsVectorLayer("usv_serie_6.shp","usv","ogr") # abrir la capa de la serie 6
for municipio in municipios.getFeatures():
    geo_mun = municipio.geometry()
    area_bosque = 0
    for poligono in serie6.getFeatures():
        geo_poli = poligono.geometry()
        cat = poligono['DESCRIPCIO']
        if "BOSQUE" in cat or "SELVA" in cat:
            if geo_poli.intersects(geo_mun):
                area_bosque += geo_poli.intersection(geo_mun).area() 
        
    porciento_bosque = round(100*(area_bosque/geo_mun.area()),1) 
    print(municipio['Nombre'], porciento_bosque, "%") 