import os
from qgis.core import QgsVectorLayer, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsProject

# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion2"

os.chdir(carpeta) # cambiar el directorio de trabajo
print() # una linea en blanco
capa = QgsVectorLayer("municipios.shp","mun","ogr") # abrir la capa

for elemento in capa.getFeatures():
    geometria = elemento.geometry()
    if "tit" in elemento['Nombre']:
        print(elemento['Nombre'])
        for elemento2 in capa.getFeatures():
            geometria2 = elemento2.geometry()
            if geometria.distance(geometria2) < 100:
                if elemento['Nombre'] != elemento2['Nombre']:
                    if "hua" in elemento2['Nombre']:
                        print("    ", elemento2['Nombre'])
                        geometria.centroid().asPoint()
                        punto = geometria.centroid()
                        layer = QgsVectorLayer('Point?crs=epsg:32614','Tesoro',"memory")
                        pr = layer.dataProvider()
                        pt = QgsFeature()
                        pt.setGeometry(punto)
                        pr.addFeatures([pt])
                        layer.updateExtents()
                        QgsProject.instance().addMapLayer(layer)
                        iface.mapCanvas().zoomToFeatureIds(layer, [1])
                        iface.mapCanvas().zoomScale(300000)
            
