# Listar las capas de una carpeta y sus respectivos ESPG y campos
#################################################################

import os
from qgis.core import QgsVectorLayer


# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion2"

os.chdir(carpeta) # cambiar el directorio de trabajo
print() # una linea en blanco
for archivo in os.listdir(carpeta): # recorre los archivos en la carpeta
    if archivo.endswith(".shp"): # filtra los .shp
        nombre = archivo[:-4] # obtiene el nombre quitando la extension
        capa = QgsVectorLayer(archivo, nombre, "ogr") # crea la capa como un objeto QgsVectorLayer 
        suESPG = capa.sourceCrs().authid() # obtiene el ESPG
        susCampos = capa.fields().names() # obtiene los nombres de los campos
        print(nombre, suESPG, susCampos) # imprime la info de la capa
        #QgsProject.instance().addMapLayer(capa)
