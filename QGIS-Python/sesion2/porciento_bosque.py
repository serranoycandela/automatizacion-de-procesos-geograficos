# Imrimir el porcentaje de bosques y selvas para cada municipio
########################################################

import os
from qgis.core import QgsVectorLayer


# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion2"

os.chdir(carpeta) # cambiar el directorio de trabajo
municipios = QgsVectorLayer("municipios.shp","mun","ogr") # abrir la capa de municipios
serie6 = QgsVectorLayer("usv_serie_6.shp","usv","ogr") # abrir la capa de la serie 6
for municipio in municipios.getFeatures(): # recorrer los elementos de la capa
    geo_mun = municipio.geometry() # obtener la geometría del municipio
    area_bosque = 0 # municipio nuevo area_bosque en 0
    for poligono in serie6.getFeatures(): # recorrer los elementos de la capa serie6
        geo_poli = poligono.geometry() # se obtiene la geometria del poligono
        cat = poligono['DESCRIPCIO'] # se obtiene la categoria del poligono 
        if "BOSQUE" in cat or "SELVA" in cat: # es poligono de bosque o selva?
            if geo_poli.intersects(geo_mun): # tiene interseccion con el municipio?
                # suma el area de la interseccion a area_bosque para este municipio
                area_bosque += geo_poli.intersection(geo_mun).area() 
    
    # calcula el porcentaje de bosque para este municipio
    porciento_bosque = round(100*(area_bosque/geo_mun.area()),1) 
    print(municipio['Nombre'], porciento_bosque, "%")     

