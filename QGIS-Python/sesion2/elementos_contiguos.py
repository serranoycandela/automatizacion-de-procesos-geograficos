# Listar los municipios contiguos de cada municipio
###################################################

import os
from qgis.core import QgsVectorLayer

# la carpeta con las capas
# en Windows los paths van con \\ ejemplo "C:\\Users\\Arturo\\pyQGIS"
carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion2"

os.chdir(carpeta) # cambiar el directorio de trabajo
print() # una linea en blanco
capa = QgsVectorLayer("municipios.shp","mun","ogr") # abrir la capa
for elemento in capa.getFeatures(): # recorrer los elementos de la capa
    geometria = elemento.geometry() # obtener la geometria
    print(elemento['Nombre']) # imprimir el valor del campo 'Nombre'
    for elemento2 in capa.getFeatures(): # recorrer de nuevo los elementos
        geometria2 = elemento2.geometry() # obtener la geometria2
        if geometria.distance(geometria2) < 100: # checar si son contiguos
            if elemento['Nombre'] != elemento2['Nombre']: # checar que no sean el mismo elemento
                print("    ", elemento2['Nombre']) # imprimir el nombre de este elemento contiguo
            
