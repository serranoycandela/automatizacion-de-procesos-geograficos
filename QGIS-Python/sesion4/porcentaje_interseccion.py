# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsField
                       )
from qgis import processing


class AreaOfIntersection(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    OVERLAY = 'OVERLAY'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return AreaOfIntersection()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'areaofintersection'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Area Of Intersection')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('LANCIS')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'lancisscripts'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("...")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.OVERLAY,
                self.tr('Overlay layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )
        

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        layer = self.parameterAsVectorLayer(
            parameters,
            self.INPUT,
            context
        )
        overlay = self.parameterAsVectorLayer(
            parameters,
            self.OVERLAY,
            context
        )
        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        if layer is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        
        if 'p_inter' not in layer.fields().names():
            layer.dataProvider().addAttributes([QgsField('p_inter', QVariant.Double, 'double', 10, 3)])
            layer.updateFields()

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(layer.sourceCrs().authid()))

        
        # Compute the number of steps to display within the progress bar and
        # get features from source
        total = layer.featureCount()
        contador = 0
        layer.startEditing()
        for f in layer.getFeatures():
            contador += 1
            geo_input = f.geometry() 
            area_interseccion = 0 
            for poligono in overlay.getFeatures(): 
                geo_poli = poligono.geometry() 
                if geo_poli.intersects(geo_input): 
                    area_interseccion += geo_poli.intersection(geo_input).area() 
            
            
            porciento_interseccion = round(100*(area_interseccion/geo_input.area()),1) 
            feedback.pushInfo("Elemento "+ str(contador))
            feedback.pushInfo("-  porcentaje de intersección "+ str(porciento_interseccion) + "%")
            feedback.setProgress(round( 100 * ( contador / total )))
            f['p_inter'] = porciento_interseccion
            layer.updateFeature(f)
        layer.commitChanges()

        
        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {'porcentajes calculados': contador}
