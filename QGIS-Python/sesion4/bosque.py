carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion4"

os.chdir(carpeta)

usv = QgsVectorLayer("usv_serie_6.shp","usv","ogr")

if 'bosque' not in usv.fields().names(): # si no tiene el campo bosque
    usv.dataProvider().addAttributes([QgsField("bosque",  QVariant.Int)]) # creamos el campo bosque
    usv.updateFields() # actualizamos los campos de la capa
    
with edit(usv): # editando la capa usv
    for f in usv.getFeatures(): # bucle sobre los elementos de la capa
        cat = f['DESCRIPCIO'] # se obtiene la categoria del poligono 
        if "BOSQUE" in cat or "SELVA" in cat: # si es bosque o selva
            f["bosque"] = 1 # el campo bosque es 1
            usv.updateFeature(f) # actualizamos el elemento f
        else:
            f["bosque"] = 0 # sino el campo bosque es 0
            usv.updateFeature(f) # actualizamos el elemento f