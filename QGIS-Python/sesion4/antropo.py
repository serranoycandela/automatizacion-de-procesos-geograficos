carpeta = "/home/fidel/GitLab/serranoycandela/automatizacion-de-procesos-geograficos/QGIS-Python/sesion4"

os.chdir(carpeta)

usv = QgsVectorLayer("usv_serie_6.shp","usv","ogr")

if 'antropo' not in usv.fields().names(): # si no tiene el campo antropo
    usv.dataProvider().addAttributes([QgsField("antropo",  QVariant.Int)]) # creamos el campo antropo
    usv.updateFields() # actualizamos los campos de la capa

with edit(usv):
    for f in usv.getFeatures():
        cat = f['DESCRIPCIO'] # se obtiene la categoria del poligono 
        if "AGRICULTURA" in cat or "PAZTISAL INDUCIDO" in cat or "URBANO CONSTRUIDO" in cat:
            f["antropo"] = 1
            usv.updateFeature(f)